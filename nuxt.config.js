const pkg = require('./package')
const PurgecssPlugin = require('purgecss-webpack-plugin')
const glob = require('glob-all')
const path = require('path')

class TailwindExtractor {
	static extract(content) {
		return content.match(/[A-z0-9-:\/]+/g) || [];
	}
}

import config from './config/site'

module.exports = {
	mode: 'universal',

	/*
	** Headers of the page
	*/
	head: {
		htmlAttrs: {
			lang: 'en'
		},
		title: 'Nuxt Skeleton',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: 'Meta description' }
		],
		link: [
			{ rel: 'icon', href: '/favicon.ico' },
			// { rel: 'stylesheet', href: '//fonts.googleapis.com/css?family=Roboto' }
			{ rel: 'preload', as: 'font', href: '/fonts/ibm-plex-sans-regular.woff2', type: 'font/woff2', crossorigin: 'true' }
		]
		// script: [
			// { src: 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', body: true }
		// ],
	},

	/*
	** Customize the progress-bar color
	*/
	// loading: { color: '#fff' },

	/*
	** Global CSS
	*/
	css: [
		'~/assets/sass/main.scss'
	],

	/*
	** Plugins to load before mounting the App
	*/
	plugins: [
		'~/plugins/globals.js'
	],

	/*
	** Nuxt.js modules
	*/
	// modules: [
	// ],

	/*
	** Build configuration
	*/
	build: {
		extractCSS: true,
		/*
		** You can extend webpack config here
		*/
		extend(config, { isDev }) {
			if (!isDev) {
				// Remove unused CSS using purgecss. See https://github.com/FullHuman/purgecss
				// for more information about purgecss.
				config.plugins.push(
					new PurgecssPlugin({
						// Specify the locations of any files you want to scan for class names.
						paths: glob.sync([
							path.join(__dirname, './pages/**/*.vue'),
							path.join(__dirname, './layouts/**/*.vue'),
							path.join(__dirname, './components/**/*.vue')
						]),
						extractors: [
							{
								extractor: TailwindExtractor,
								// Specify the file extensions to include when scanning for
								// class names.
								extensions: ["html", "vue"]
							}
						],
						whitelist: [
							'html',
							'body',
							'h1',
							'h2',
							'h3',
							'p',
							'blockquote',
						],
						whitelistPatterns: [/\bhljs\S*/]
					})
				)
			}
		}
	}
}
