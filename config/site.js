export default {
    site: {
        name: 'Skeleton',
        address_line_1: '42 - 46 Baldwin Street',
        email: 'info@skeleton.co.uk',
        phone: '0117 325 0200'
    },
    socials: {
        facebook: 'https://facebook.com',
        twitter: 'https://twitter.com',
        linkedin: 'https://linkedin.com'
    }
}
